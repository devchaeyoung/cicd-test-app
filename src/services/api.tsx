import axios from "axios";

export const api = axios.create({
  baseURL: "http://43.200.136.37:8080/",
  withCredentials: true,
});
