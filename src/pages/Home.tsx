import { Link } from "react-router-dom";
import { Form, Input, Inputs, Title, Wrapper } from "../components/Common";
import styled from "styled-components";
const Home = () => {
  return (
    <Wrapper>
      <Title>로그인하기</Title>
      <Form>
        <Inputs>
          <Input placeholder="아이디" />
          <Input placeholder="비밀번호" type="password" />
        </Inputs>
        <Button>Login</Button>
      </Form>
      <CommonLink to="/signup">회원가입하기</CommonLink>
    </Wrapper>
  );
};

export default Home;

const Button = styled.button`
  background-color: black;
  color: white;
  padding: 20px;
  border-radius: 10px;
`;

const CommonLink = styled(Link)`
  color: black;
  text-decoration: none;
  &:visited {
    color: black;
    text-decoration: none;
  }
`;
